/* --------------------------------------------------------------------------- */
/* Includes
/* --------------------------------------------------------------------------- */
var gulp                = require('gulp'),
    sass                = require('gulp-sass'),
    gutil               = require('gulp-util'),
    rename              = require('gulp-rename'),
    uglify              = require('gulp-uglify'),
    concat              = require('gulp-concat'),
    gulpif              = require('gulp-if'),
    stylus              = require('gulp-stylus'),
    sourcemaps          = require('gulp-sourcemaps'),
    newer               = require('gulp-newer'),
    spritesmith         = require('gulp.spritesmith'),
    fs                  = require('fs'),
    svgSprite           = require("gulp-svg-sprites"),
    cleanCSS            = require('gulp-clean-css'),
    plumber             = require('gulp-plumber'),
    path                = require('path'),
    stripCssComments    = require('gulp-strip-css-comments'),
    autoprefixer        = require('gulp-autoprefixer');





/* --------------------------------------------------------------------------- */
/* Defaults
/* --------------------------------------------------------------------------- */
var defaults = {
    css: {
        source      : 'assets/styles/scss',
        destination : 'dist/styles/css',
        extension   : '.scss'
    },
    js_root: {
        source      : 'assets/scripts',
        destination : 'dist/scripts',
        extension   : '.js'
    },
    js_head: {
        source      : 'assets/scripts/head',
        destination : 'dist/scripts',
        extension   : '.js'
    },
    js_footer: {
        source      : 'assets/scripts/footer',
        destination : 'dist/scripts',
        extension   : '.js'
    },
    sprite: {
        source      : 'assets/images/sprite',
        destination : 'dist/images',
        extension   : '.png'
    },
    svgsprite: {
        source      : 'assets/images/svg',
        destination : 'assets/styles/scss/svg/',
        extension   : 'svg'
    }
};



/* --------------------------------------------------------------------------- */
/* Compile CSS
/* --------------------------------------------------------------------------- */
gulp.task('sass', function () {  
    
    gulp.src(defaults.css.source+'/*'+defaults.css.extension)
    .pipe(plumber())
    .pipe(stripCssComments())
    .pipe(sass({includePaths: ['./css']}))
    .on('error', onError )
    // .pipe(cleanCSS({debug: true}, function(details) {
    //     console.log(details.name + ': ' + details.stats.originalSize);
    //     console.log(details.name + ': ' + details.stats.minifiedSize);
    // }))
    .pipe(concat('app.css'))
    .pipe(autoprefixer({
        browsers: ['last 2 versions'],
        cascade: false
    }))
    .pipe(gulp.dest(defaults.css.destination));

});

// gulp.src(['assets/images/**/*']).pipe(gulp.dest('dist/images'));

/* --------------------------------------------------------------------------- */
/* Watch
/* --------------------------------------------------------------------------- */
gulp.task('watch', function() {

    // css
    gulp.watch(defaults.css.source + '/**/*' + defaults.css.extension, ['sass']);

    // js
    gulp.watch(defaults.js_head.source + '/*' + defaults.js_head.extension, ['concat_js']);
    gulp.watch(defaults.js_footer.source + '/*' + defaults.js_footer.extension, ['concat_js']);

    // svg sprite
    gulp.watch(defaults.svgsprite.source + '/*' + defaults.svgsprite.extension, ['svgsprite']);

    // png sprite
    gulp.watch(defaults.sprite.source + '/*' + defaults.sprite.extension, ['sprite']);

});





/* --------------------------------------------------------------------------- */
/* Uglify js
/* --------------------------------------------------------------------------- */

gulp.task('concat_js', function() {

    // head
    gulp.src([
        //order
        defaults.js_head.source + '/lib/jquery-1.11.3.min.js',
        // defaults.js_head.source + '/lib/owl.carousel.min.js',
        // defaults.js_head.source + '/lib/jquery.browser.min.js',
        // defaults.js_head.source + '/lib/jquery.ba-hashchange.min.js',
        // defaults.js_head.source + '/lib/bootstrap.bundle.js',
        // defaults.js_head.source + '/lib/modernizr.all.min.js',
        // defaults.js_head.source + '/lib/TweenMax.min.js',
        // defaults.js_head.source + '/lib/ScrollMagic.js',
        // defaults.js_head.source + '/lib/animation.gsap.min.js',
        // defaults.js_head.source + '/lib/swiper.min.js',
        // defaults.js_head.source + '/lib/map/swiper.min.js.map',
        // defaults.js_head.source + '/lib/moment.min.js',
        // defaults.js_head.source + '/lib/daterangepicker.js',
        // defaults.js_head.source + '/lib/*.js',
        //defaults.js_head.source + '/lib/*.js',
        defaults.js_head.source + '/*.js'
    ])
        .pipe(concat('app-head.js'))
        .pipe(gulp.dest(defaults.js_head.destination));


    // footer
    gulp.src([
        //order
        defaults.js_footer.source + '/lib/*.js',
        defaults.js_footer.source + '/*.js',
        'react/public/*.js'
    ])
        .pipe(concat('app-footer.js'))
        .pipe(gulp.dest(defaults.js_footer.destination));
});




// --------------------------------------------------------------------------- //
// Sprits
// --------------------------------------------------------------------------- //
gulp.task('sprite', function() {
    var spriteData = gulp.src( defaults.sprite.source + '/*' + defaults.sprite.extension)
        .pipe(spritesmith({

            //retina setup
            /*
            retinaSrcFilter: defaults.sprite.source + '/*-2x' + defaults.sprite.extension,
            imgName: 'sprite.png',
            retinaImgName: 'sprite-2x.png',
            imgPath: '/images/sprite.png',
            retinaImgPath: '/images/sprite-2x.png',
            cssName: '_sprites.scss'
            */


            //non retina setup
            imgName: 'sprite.png',
            imgPath: '../../../dist/images/sprite.png',
            cssName: '_sprites.scss',
            cssFormat: 'scss_maps'
        }));

    spriteData.img.pipe( gulp.dest( defaults.sprite.destination ) );
    spriteData.css.pipe( gulp.dest( defaults.css.source ) );
});

/* --------------------------------------------------------------------------- */
/* SVG sprite
/* --------------------------------------------------------------------------- */
gulp.task('svgsprite', function () {
    //return gulp.src('images/svg/*.svg')
    return gulp.src( defaults.svgsprite.source + '/*' + defaults.svgsprite.extension )
        .pipe(svgSprite({
            preview: false,
            templates: {
                // scss: true
                scss: fs.readFileSync("assets/styles/scss/svg-tpl/svgsprite_template.scss", "utf-8")
            },
            svg: {
                sprite: "svg-sprite.svg"
            },
            cssFile: "svg-sprites.scss",     // Custom path and name
            // svgPath:  "assets/styles/scss/svg/%f"
            svgPath:   "../scss/svg/%f"
        }))
        .pipe(gulp.dest( defaults.svgsprite.destination ));
});




/* --------------------------------------------------------------------------- */
/* Init
/* --------------------------------------------------------------------------- */
//gulp.task('default', ['sass', 'compress', 'concat', 'watch']); 
//gulp.task('default', ['sass', 'sprite', 'concat_js', 'watch']); 
gulp.task('default', ['sass', 'sprite', 'svgsprite', 'concat_js', 'watch']);
// gulp.task('fixdb', ['update_mysql_with_hostname']);
// gulp.task('default', ['sass', 'sprite', 'svgsprite', 'concat_js', 'compress', 'watch']);


/* --------------------------------------------------------------------------- */
/* Functions
/* --------------------------------------------------------------------------- */
var onError = function (err) {  
  gutil.beep();
  console.log(err);
};