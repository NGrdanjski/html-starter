# Requirement
For developing you need some staff. Install them in the following order.

1. Nodejs - https://nodejs.org/en/
2. Npm package manager - https://www.npmjs.com/package/npm-install-global
3. Yarn - https://yarnpkg.com/lang/en/docs/install/
4. Gulp - https://www.npmjs.com/package/gulp-install#installation


# Start Development
1. First open the CMD or terinal app on your pc/mac machine.
2. Navigate to your project folder ```$ cd ../project_folder/```
3. Run ```$ yarn install```
4. Run ```$ gulp```

-- When you run gulp comand, gulp will start compaile your assets folder to dist folder. And will watch assets folder for changes in this files, then automatic compile changes.

 # Production
 When you push file to production you only push /dist folder NOT! /assets and not node_modules. yarn.lock, package.json and gulp.js files also not push to production.
 
 
 If you have some questions please let me  know =>  [nikolag@1i0.hr](mailto:nikolag@1i0.hr)